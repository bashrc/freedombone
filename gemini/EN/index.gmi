``` title
░█▀▀▀ █▀▀█ █▀▀ █▀▀ █▀▀▄ █▀▀█ █▀▄▀█ █▀▀▄ █▀▀█ █▀▀▄ █▀▀ 
░█▀▀▀ █▄▄▀ █▀▀ █▀▀ █──█ █──█ █─▀─█ █▀▀▄ █──█ █──█ █▀▀ 
░█─── ▀─▀▀ ▀▀▀ ▀▀▀ ▀▀▀─ ▀▀▀▀ ▀───▀ ▀▀▀─ ▀▀▀▀ ▀──▀ ▀▀▀
```

## Your Data.  Your Server.  Your Place.

The internet is becoming uninhabitable. Centralized, bloated with ads, utterly hostile to privacy, choked by toxicity and monopolized by billionaires. But perhaps this isn't where the internet dies. Perhaps a radically decentralized future can emerge from the moribund wreckage of the present.

Freedombone is a home server system which enables you to run your own internet services, individually or as a household. It includes all of the things you'd expect such as email, chat, VoIP, web sites, wikis, blogs, social networks, media hosting and more. You can run Freedombone on an old laptop or single board computer. No ads and no built-in spying.

=> install_desktop.gmi ► Desktop/Laptop
=> install_sbc.gmi ► Single Board Computer
=> mesh.gmi ► Mesh version
=> admin.gmi ► Admin Guide
=> apps.gmi ► Apps
=> devguide.gmi ► Developer Guide
=> faq.gmi ► FAQ
=> blog/index.gmi ► Gemlog
=> sites.gmi ► Various gemini sites

Want to keep this project going? Donate!

=> https://www.patreon.com/freedombone ► Patreon
=> https://paypal.me/BobMottram ► Paypal
=> https://liberapay.com/bashrc ► Liberapay


