=> mesh.gmi ⯇ Go Back

# Build a Community Network with Freedombone

If you've read the preceding sections about deploying disk images then you know the technology part. Now comes the harder task which is to roll out a network system over a geographical area.

## Scope your network area

What area do you want to cover? Draw it out on a map. Within your area which are the best locations for mesh routers. If you're using cantennas or other high gain antennas what's their maximum range with a clear line of sight?

Map out the locations or rooftops you ideally would like to use and which will give good coverage.

How durable does the network need to be? Is this emergency/temporary coverage, or intended to be permanent infrastructure? This will determine what type of hardware to use and what type of discussion's you'll be having.

## Engage with the community

In your target areas run surveys or hold meetings to establish who is willing to participate. Avoid setting the expectation that this is a free as in gratis network since it will take effort and some (hopefully minimal) amount of money to deploy and maintain over time. It may be helpful to prepare leaflets or posters explaining the costs and benefits in a manner that your community is likely to identify with.

## Make an install plan

This should be an inventory of hardware and software, and a step-by-step instructions which members of your community can follow. Make an install package containing the hardware and instructions which can be repeadably deployed. This need not be highly sophisticated, and could comprise of some mesh images on USB sticks, maybe some wifi antennas, and instructions on what to do with them.

## Hold training events

Hold some events which train people how to install, use and maintain the system. These can be useful for ironing out any bugs in the install plan and also for determining what are the best ways to engage with the community. Avoid appearing like some Silicon Valley stooge pushing the latest trendy gadget or being the high-minded liberal who is going to deliver "civilization" to the colonies. Intentions are less important than results. Be as realistic as possible about what it takes to keep the system running and listen to people who know the locality. This is as much a human network as it is a technical one.

## Have a maintenance process

Maybe people should check that things are still working every so often. If a fault happens there needs to be some process to follow or people to contact, otherwise entropy will eventually kill the network.
