#!/bin/bash
#  _____               _           _
# |   __|___ ___ ___ _| |___ _____| |_ ___ ___ ___
# |   __|  _| -_| -_| . | . |     | . | . |   | -_|
# |__|  |_| |___|___|___|___|_|_|_|___|___|_|_|___|
#
#        Your Data.  Your Server.  Your Place.
#
# set the language
#
# License
# =======
#
# Copyright (C) 2018-2020 Bob Mottram <bob@freedombone.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

PROJECT_NAME='freedombone'

language="$1"

if [ ! "$language" ]; then
    exit 1
fi

CONFIGURATION_FILE="/root/${PROJECT_NAME}.cfg"

SECONDS=0

source /usr/local/bin/${PROJECT_NAME}-shortcuts

UTILS_FILES="/usr/share/${PROJECT_NAME}/utils/${PROJECT_NAME}-utils-*"
for f in $UTILS_FILES
do
    source "$f"
done

echo "Including source files took $SECONDS seconds"
SECONDS=0

if ! grep -q "DEFAULT_LANGUAGE=" "$CONFIGURATION_FILE"; then
    echo "DEFAULT_LANGUAGE=$language" >> "$CONFIGURATION_FILE"
else
    sed -i "s|DEFAULT_LANGUAGE=.*|DEFAULT_LANGUAGE=$language|g" "$CONFIGURATION_FILE"
fi
locale-gen "${language}"
update-locale LANG="${language}"
update-locale LANGUAGE="${language}"
update-locale LC_MESSAGES="${language}"
update-locale LC_ALL="${language}"
update-locale LC_CTYPE="${language}"

echo "Changing locale took $SECONDS seconds"
SECONDS=0

local_hostname=$(grep 'host-name' /etc/avahi/avahi-daemon.conf | awk -F '=' '{print $2}').local
webadmin_install_dir="/var/www/${local_hostname}/htdocs/admin"
webadmin_home_dir="/var/www/${local_hostname}/htdocs/home"

cp /etc/rssgarden/feeds_edit.html "$webadmin_install_dir/feeds_edit.html"
chown www-data:www-data "$webadmin_install_dir/feeds_edit.html"
if ! grep -q 'feeds_edit.html' "$webadmin_install_dir/feeds.html"; then
    cp /etc/rssgarden/feeds_edit.html "$webadmin_install_dir/feeds.html"
    chown www-data:www-data "$webadmin_install_dir/feeds.html"
fi

echo "Copying rss feeds edit screens took $SECONDS seconds"
SECONDS=0

android_update_apps
echo "Updating apps took $SECONDS seconds"

# set language in search
language_short="${language:0:2}"
sed -i "s|\"search_language\".*|\"search_language\" value=\"${language_short}\">|g" "$webadmin_install_dir/index.html"
chown www-data:www-data "$webadmin_install_dir/index.html"
sed -i "s|\"search_language\".*|\"search_language\" value=\"${language_short}\">|g" "$webadmin_home_dir/index.html"
chown www-data:www-data "$webadmin_home_dir/index.html"

set_rss_feeds_header

echo "Setting rss feeds screen header took $SECONDS seconds"
SECONDS=0

# title on community network screen
if [ -f "$webadmin_install_dir/network.html" ]; then
    translated_network_str=$(web_admin_translate_text "Choose your network:")
    sed -i "s|<p class=\"headertext\".*|<p class=\"headertext\" translate=\"yes\">${translated_network_str}</p>|g" "$webadmin_install_dir/network.html"
fi

echo "Set community network screen title took $SECONDS seconds"
SECONDS=0

# change the selected language in the web interface
cp "$webadmin_install_dir/language_template.html" "$webadmin_install_dir/language.html"
sed -i "s|\"$language\" translate=\"yes\"|\"$language\" translate=\"yes\" selected|g" "$webadmin_install_dir/language.html"
chown www-data:www-data "$webadmin_install_dir/language.html"

echo "Changing selected language in web interface took $SECONDS seconds"

# change the waiting screen to settings
cp "$webadmin_install_dir/settings.html" "$webadmin_install_dir/language_waiting.html"
chown www-data:www-data "$webadmin_install_dir/language_waiting.html"

echo $"Language changed to $language"

exit 0
